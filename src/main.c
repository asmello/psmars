#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#include "img.h"

int main (int argc, char **argv) {
    FILE *in = NULL, *out = NULL;
    img_t img;
    img_init(&img);
    if (argc != 3) {
        printf("Usage: %s <input file> <output file>\n", argv[0]);
        return -1;
    }
    double start = omp_get_wtime();;
    if ((in = fopen(argv[1], "rb")) == NULL) {
        perror(argv[1]);
        return -1;
    }
    if (img_read(in, &img, NULL) != 0) {
        perror("failed to read the image");
        return -1;
    }
    fclose(in);
    if (img_smooth(&img) != 0) {
        perror("failed to process the image");
        return -1;
    }
    if ((out = fopen(argv[2], "wb+")) == NULL) {
        perror(argv[2]);
        return -1;
    }
    if (img_write(out, &img, NULL) != 0) {
        perror("failed to write the image");
        return -1;
    }
    double end = omp_get_wtime();
    printf("[i] Total time taken: %lf secs.\n", end - start);
    img_destroy(&img);
    fclose(out);
    return 0;
}
