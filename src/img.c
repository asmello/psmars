#include "img.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h> // isspace

#define BSIZE 512
#define NBORS 2

void img_init(img_t *img)
{
    memset(img, 0, sizeof(img_t));
}

void img_destroy(img_t *img)
{
    free(img->p);
    free(img->r);
}

#pragma omp declare simd notinbranch uniform(r, k, bsize)
static inline size_t get_index(const rect_t *r, size_t k,
    long long int x, long long int y, size_t bsize)
{
    return (x + r[k].x) * (r[k].w + 2*bsize) + (y + r[k].y);
}

#pragma omp declare simd notinbranch linear(buf:2)
static inline uint16_t be16_to_cpu(const uint8_t *buf)
{
    return ((uint16_t)buf[1]) | (((uint16_t)buf[0]) << 8);
}

#pragma omp declare simd notinbranch linear(buf:2)
static inline void cpu_to_be16(uint8_t *buf, uint16_t val)
{
    buf[0] = (val & 0xFF00) >> 8;
    buf[1] = (val & 0x00FF);
}

static int skip(FILE *f)
{
    int c;
    do {
        c = fgetc(f);
        if (c == '#') {
            do {
                c = fgetc(f);
            } while (c != EOF && c != '\n');
        }
    } while (isspace(c));
    return ungetc(c, f);
}

int img_read(FILE *in, img_t *img, const rect_t *sec)
{
    if (skip(in) == EOF) {
        return -1;
    }
    char magic[3]; // Parse format identifier
    if (fscanf(in, "%2s", magic) != 1) {
        return -1;
    }
    // Parse image metadata
    size_t width, height;
    uint16_t maxval;
    if (skip(in) == EOF) {
        return -1;
    }
    if (fscanf(in, "%zu", &width) != 1) {
        return -1;
    }
    if (skip(in) == EOF) {
        return -1;
    }
    if (fscanf(in, "%zu", &height) != 1) {
        return -1;
    }
    if (skip(in) == EOF) {
        return -1;
    }
    if (fscanf(in, "%hu", &maxval) != 1) {
        return -1;
    }
    if (skip(in) == EOF) {
        return -1;
    }
    rect_t scpy; // verify bounds
    if (sec == NULL) {
        scpy.x = scpy.y = 0;
        scpy.w = width;
        scpy.h = height;
    } else {
        if (sec->y + sec->w > width || sec->x + sec->h > height) {
            return -1;
        }
        memcpy(&scpy, sec, sizeof(rect_t));
    }
    if (strcmp(magic, "P5") == 0) { // grayscale
        size_t pixels = scpy.w * scpy.h;
        if (maxval < 256) { // single byte image (8 bits/pixel)
            void *aux = realloc(img->p, pixels);
            if (aux == NULL) {
                return -1;
            }
            img->p = (uint8_t *) aux; // set new pixel buffer
            // Skip to section start
            if (fseek(in, scpy.x * scpy.w + scpy.y, SEEK_CUR)) {
                return -1;
            }
            // Copy the desired image section
            for (size_t x = 0; x < scpy.h; ++x) {
                if (fread(img->p + x * scpy.w, 1, scpy.w, in)
                    != scpy.w) {
                    return -1;
                }
            }
            aux = realloc(img->r, sizeof(rect_t)); // single image
            if (aux == NULL) {
                return -1;
            }
            img->r = (rect_t *) aux;
            // Set the copied image's metadata
            img->r[0].x = img->r[0].y = 0;
            img->r[0].h = scpy.h;
            img->r[0].w = scpy.w;
            img->flags = IMG_GRAYSCALE;
            img->maxv = maxval;
            img->n = 1;
        } else { // 2 byte image (16 bits/pixel)
            void *aux = realloc(img->p, 2 * pixels);
            if (aux == NULL) {
                return -1;
            }
            img->p = (uint8_t *) aux; // set new pixel buffer
            // Skip to section start
            if (fseek(in, 2 * (scpy.x * scpy.w + scpy.y), SEEK_CUR)) {
                return -1;
            }
            // Copy the desired image section
            for (size_t x = 0; x < scpy.h; ++x) {
                // Read an entire row
                if (fread(img->p + 2 * x * scpy.w, 2, scpy.w, in)
                    != scpy.w) {
                    return -1;
                }
                // Byteswap for endianness fix
                uint16_t *p16 = (uint16_t *) img->p;
                #pragma omp simd
                for (size_t y = 0; y < scpy.w; ++y) {
                    p16[x * scpy.w + y] =
                                    be16_to_cpu(img->p + 2 * (x * scpy.w + y));
                }
            }
            aux = realloc(img->r, sizeof(rect_t)); // single image
            if (aux == NULL) {
                return -1;
            }
            // Set the copied image's metadata
            img->r = (rect_t *) aux;
            img->r[0].x = img->r[0].y = 0;
            img->r[0].h = scpy.h;
            img->r[0].w = scpy.w;
            img->flags = IMG_GRAYSCALE16;
            img->maxv = maxval;
            img->n = 1;
        }
        return 0;
    } else if (strcmp(magic, "P6") == 0) { // If color image
        size_t pixels = scpy.w * scpy.h;
        if (maxval < 256) { // single byte image (8 bits/pixel)
            void *aux = realloc(img->p, 3 * pixels); // 3 channels image
            if (aux == NULL) {
                return -1;
            }
            img->p = (uint8_t *) aux; // set new pixel buffer
            // Skip to section start
            if (fseek(in, 3 * (scpy.x * scpy.w + scpy.y), SEEK_CUR)) {
                return -1;
            }
            aux = realloc(img->r, 3 * sizeof(rect_t)); // 3 channels image
            if (aux == NULL) {
                return -1;
            }
            // Set the copied image's metadata
            img->r = (rect_t *) aux;
            for (size_t i = 0; i < 3; ++i) {
                img->r[i].x = i*scpy.h;
                img->r[i].y = 0;
                img->r[i].h = scpy.h;
                img->r[i].w = scpy.w;
            }
            img->flags = IMG_COLOR_RGB;
            img->maxv = maxval;
            img->n = 3;
            uint8_t *aux_row = (uint8_t *) malloc(3 * scpy.w);
            // Copy the desired image section
            for (size_t x = 0; x < scpy.h; ++x) {
                if (fread(aux_row, 1, 3 * scpy.w, in) != (3 * scpy.w)) {
                    return -1;
                }
                for (size_t y = 0; y < scpy.w; ++y) { // spread the colors
                    img->p[get_index(img->r, 0, x, y, 0)] = aux_row[3 * y];
                    img->p[get_index(img->r, 1, x, y, 0)] = aux_row[3 * y + 1];
                    img->p[get_index(img->r, 2, x, y, 0)] = aux_row[3 * y + 2];
                }
            }
            free(aux_row);
        } else { // 2 byte image (16 bits/pixel)
            void *aux = realloc(img->p, 6 * pixels); // 3 channels image
            if (aux == NULL) {
                return -1;
            }
            img->p = (uint8_t *) aux; // set new pixel buffer
            // Skip to section start
            if (fseek(in, 6 * (scpy.x * scpy.w + scpy.y), SEEK_CUR)) {
                return -1;
            }
            aux = realloc(img->r, 3 * sizeof(rect_t)); // single image
            if (aux == NULL) {
                return -1;
            }
            // Set the copied image's metadata
            img->r = (rect_t *) aux;
            for (size_t i = 0; i < 3; ++i) {
                img->r[i].x = i*scpy.h;
                img->r[i].y = 0;
                img->r[i].h = scpy.h;
                img->r[i].w = scpy.w;
            }
            img->flags = IMG_COLOR_RGB16;
            img->maxv = maxval;
            img->n = 3;

            uint8_t *aux_row = (uint8_t *) malloc(6 * scpy.w);
            // Copy the desired image section
            for (size_t x = 0; x < scpy.h; ++x) {
                // Read an entire row
                if (fread(aux_row, 2, 3 * scpy.w, in) != (3 * scpy.w)) {
                    return -1;
                }
                // Byteswap for endianness fix
                uint16_t *p16 = (uint16_t *) img->p;
                for (size_t y = 0; y < scpy.w; ++y) { // spread the colors
                    p16[get_index(img->r, 0, x, y, 0)] =
                                              be16_to_cpu(aux_row + 2 * 3 * y);
                    p16[get_index(img->r, 1, x, y, 0)] =
                                        be16_to_cpu(aux_row + 2 * (3 * y + 1));
                    p16[get_index(img->r, 2, x, y, 0)] =
                                        be16_to_cpu(aux_row + 2 * (3 * y + 2));
                }
            }
            free(aux_row);
        }
        return 0;
    } else {
        return -1;
    }
}

int img_write(FILE *out, img_t *img, const rect_t *sec)
{

    rect_t scpy;
    if (sec == NULL) {
        if (img->r == NULL) { // empty image (nothing can be written)
            return -1;
        }
        memcpy(&scpy, img->r, sizeof(rect_t));
    }  else {
        if (sec->y + sec->w > img->r[0].w || sec->x + sec->h > img->r[0].h) {
            return -1;
        }
        memcpy(&scpy, sec, sizeof(rect_t));
    }
    // Set actual bounds
    scpy.x += img->r[0].x;
    scpy.y += img->r[0].y;
    if (img->flags & (IMG_COLOR_RGB | IMG_COLOR_RGB16)) {
        // Write image metadata
        if (fprintf(out, "P6\n%zu %zu\n%hu\n", scpy.w, scpy.h, img->maxv) < 0) {
            return -1;
        }
        if (img->flags & IMG_COLOR_RGB16) {
            uint8_t *row = (uint8_t *) malloc(6 * scpy.w);
            for (size_t x = 0; x < scpy.h; ++x) {
                // Make a byteswapped copy of row x (to fix endianness)
                uint16_t *p16 = (uint16_t *) img->p;
                #pragma omp simd
                for (size_t y = 0; y < scpy.w; ++y) {
                    cpu_to_be16(row + 2 * (3 * y),
                                            p16[get_index(img->r, 0, x, y, 0)]);
                    cpu_to_be16(row + 2 * (3 * y + 1),
                                            p16[get_index(img->r, 1, x, y, 0)]);
                    cpu_to_be16(row + 2 * (3 * y + 2),
                                            p16[get_index(img->r, 2, x, y, 0)]);
                }
                // Write row into file
                if (fwrite(row, 2, 3*scpy.w, out) != (3*scpy.w)) {
                    return -1;
                }
            }
            free(row);
        } else {
            uint8_t *row = (uint8_t *) malloc(3*scpy.w);
            for (size_t x = 0; x < scpy.h; ++x) {
                // Bufferize current row
                for (size_t y = 0; y < scpy.w; ++y) {
                    row[3 * y] = img->p[get_index(img->r, 0, x, y, 0)];
                    row[3 * y + 1] = img->p[get_index(img->r, 1, x, y, 0)];
                    row[3 * y + 2] = img->p[get_index(img->r, 2, x, y, 0)];
                }
                // Write row into file
                if (fwrite(row, 1, 3*scpy.w, out) != (3*scpy.w)) {
                    return -1;
                }
            }
            free(row);
        }
    } else if (img->flags & (IMG_GRAYSCALE | IMG_GRAYSCALE16)) {
        // Write image metadata
        if (fprintf(out, "P5\n%zu %zu\n%hu\n", scpy.w, scpy.h, img->maxv) < 0) {
            return -1;
        }
        if (img->flags & IMG_GRAYSCALE16) {
            uint8_t *row = (uint8_t *) malloc(2 * scpy.w);
            for (size_t x = 0; x < scpy.h; ++x) {
                // Make a byteswapped copy of row x (to fix endianness)
                uint16_t *p16 = (uint16_t *) img->p;
                #pragma omp simd
                for (size_t y = 0; y < scpy.w; ++y) {
                    cpu_to_be16(row + 2 * y, p16[get_index(&scpy, 0, x, y, 0)]);
                }
                // Write row into file
                if (fwrite(row, 2, scpy.w, out) != scpy.w) {
                    return -1;
                }
            }
            free(row);
        } else {
            for (size_t x = 0; x < scpy.h; ++x) {
                // Write row into file
                if (fwrite(img->p + get_index(&scpy, 0, x, 0, 0),
                    1, scpy.w, out) != scpy.w) {
                    return -1;
                }
            }
        }
    }
    return 0;
}

int img_border_zero(img_t *dest, const img_t *orig, size_t len)
{
    size_t psize = orig->flags & IMG_DEPTH16 ? 2 : 1;
    size_t num_pixels = 0;
    for (size_t k = 0; k < orig->n; ++k) { // calculate total pixels
        num_pixels += (orig->r[k].h + 2*len) * (orig->r[k].w + 2*len);
    }
    void *aux = realloc(dest->p, num_pixels * psize);
    if (aux == NULL) {
        return -1;
    }
    memset(aux, 0, num_pixels * psize); // to improve: set all pixels to zero
    dest->p = (uint8_t *) aux;
    aux = realloc(dest->r, orig->n * sizeof(rect_t));
    if (aux == NULL) {
        return -1;
    }
    dest->r = (rect_t *) aux; // new regions
    size_t row = len;
    for (size_t k = 0; k < orig->n; ++k) {
        dest->r[k].y = len;
        dest->r[k].x = row;
        dest->r[k].w = orig->r[k].w;
        dest->r[k].h = orig->r[k].h;
        #pragma omp parallel for
        for (size_t x = 0; x < orig->r[k].h; ++x) { // Copy original row by row
            memcpy(dest->p + psize * get_index(dest->r, k, x, 0, len),
                   orig->p + psize * get_index(orig->r, k, x, 0, 0),
                   orig->r[k].w * psize);
        }
        row += orig->r[k].h + 2*len;
    }
    dest->flags = orig->flags;
    dest->maxv = orig->maxv;
    dest->n = orig->n;
    return 0;
}

int img_smooth(img_t *img)
{
    img_t img_aux;
    img_init(&img_aux);
    if (img_border_zero(&img_aux, img, NBORS) < 0) {
        return -1;
    }
    if (img->flags & IMG_DEPTH16) {
        uint16_t *p16 = (uint16_t *) img->p;
        uint16_t *p16_aux = (uint16_t *) img_aux.p;
        for (int k = 0; k < img->n; ++k) {
            #pragma omp parallel for
            for (size_t x = 0; x < img->r[k].h; ++x) {
                for (size_t y = 0; y < img->r[k].w; ++y) {
                    int val = 0;
                    #pragma omp simd collapse(2) reduction(+:val)
                    for (long long int i = x-NBORS;
                         i <= (long long int) x+NBORS; ++i) {
                        for (long long int j = y-NBORS;
                             j <= (long long int) y+NBORS; ++j) {
                            val += p16_aux[
                                get_index(img_aux.r, k, i, j, NBORS)];
                        }
                    }
                    val /= (2*NBORS+1)*(2*NBORS+1);
                    p16[get_index(img->r, k, x, y, 0)] = val;
                }
            }
        }
    } else {
        for (int k = 0; k < img->n; ++k) {
            #pragma omp parallel for
            for (size_t x = 0; x < img->r[k].h; ++x) {
                for (size_t y = 0; y < img->r[k].w; ++y) {
                    int val = 0;
                    #pragma omp simd collapse(2) reduction(+:val)
                    for (long long int i = x-NBORS;
                         i <= (long long int) x+NBORS; ++i) {
                        for (long long int j = y-NBORS;
                             j <= (long long int) y+NBORS; ++j) {
                            val += img_aux.p[
                                get_index(img_aux.r, k, i, j, NBORS)];
                        }
                    }
                    val /= (2*NBORS+1)*(2*NBORS+1);
                    img->p[get_index(img->r, k, x, y, 0)] = val;
                }
            }
        }
    }
    img_destroy(&img_aux);
    return 0;
}
