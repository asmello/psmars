#ifndef IMG_H
#define IMG_H

#include <stdint.h>
#include <stdio.h>

typedef struct { size_t x, y, w, h; } rect_t;

typedef struct {
    uint16_t n; // total stored images (might be channels)
    uint16_t maxv; // greatest possible value for a pixel (related to bit depth)
    uint8_t flags; // image metadata
    uint8_t *p; // all the pixels
    rect_t *r; // effective image regions
} img_t;

#define IMG_GRAYSCALE 0x1
#define IMG_COLOR_RGB 0x2
#define IMG_GRAYSCALE16 0x04
#define IMG_COLOR_RGB16 0x08
#define IMG_DEPTH16 (IMG_GRAYSCALE16 | IMG_COLOR_RGB16)

/* Section is the region of the image to be read or written.
 * NULL is equivalent to "entire image"
 */
void img_init(img_t *img);
int img_read(FILE *in, img_t *img, const rect_t *section);
int img_write(FILE *out, img_t *img, const rect_t *section);
int img_smooth(img_t *img);
int img_border_zero(img_t *dest, const img_t *orig, size_t len);
void img_destroy(img_t *img);

#endif
