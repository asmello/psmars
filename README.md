Para compilar e executar o programa:

# Versão serial 
0. Estar no Branch Master ou MPI, ou com os respectivos arquivos
1. Remover (se existir) flag "-fopenmp" no Makefile (linha 14, FFLAGS)
2. Linha de comando: make
3. Linha de comando: bin/psmars <entrada> <saída>

# Versão paralela simples (apenas thread)
0. Estar no Branch Master ou MPI, ou com os respectivos arquivos
1. Colocar (se não existir) flag "-fopenmp" no Makefile (linha 14, FFLAGS)
2. Linha de comando: make
3. Linha de comando: bin/psmars <entrada> <saída>

# Versão distribuída
0. Estar no Branch MPI, ou com os respectivos arquivos
1. Colocar ou não "-fopenmp" no Makefile (linha 14, FFLAGS), de acordo com o desejo
de ter o paralelismo local. Para a versão paralela completa, ativá-la.
2. Linha de comando: make
3. Linha de comando: mpirun -pernode --hostfile <arquivo_host> bin/psmars <entrada> <saída>

# Versão cuda
0. Estar no Branch cuda, ou com os respectivos arquivos
1. Linha de comando: make
2. Linha de comando: bin/psmars <entrada> <saída>